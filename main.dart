//import 'dart:convert';
//
//import 'apigateway_event.dart';
//import 'runtime.dart';
//
//void main() async {
//  await Runtime()
//    ..registerHandler(
//      'squareNumber',
//      squareNumber,
//    )
//    ..registerApiGatewayHandler(
//      'apiGatewaySquareNumber',
//      apiGatewaySquareNumber,
//    )
//    ..start();
//}
//
//Future<Map<String, dynamic>> squareNumber(
//  Map<String, dynamic> event,
//  Context context,
//) async {
//  final squaredNumber = event['number'] * event['number'];
//  return {'result': squaredNumber};
//}
//
//Future<ApiGatewayResponse> apiGatewaySquareNumber(
//  ApiGatewayEvent apiRequest,
//  Context context,
//) async {
//  final Map<String, dynamic> asJson =
//      json.decode(apiRequest.body) as Map<String, dynamic>;
//  final squaredNumber = asJson['number'] * asJson['number'];
//
//  return ApiGatewayResponse(
//    body: json.encode({'result': squaredNumber}),
//    isBase64Encoded: false,
//    statusCode: 200,
//  );
//}
