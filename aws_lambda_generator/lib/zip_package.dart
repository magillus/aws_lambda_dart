import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:build/build.dart';
import 'package:resource/resource.dart' as resource;

PostProcessBuilder awsZip(BuilderOptions options) {
  print("Running ScriptBuilder - $options");
  return ScriptBuilder();
}

class ScriptBuilder extends PostProcessBuilder {
  @override
  Iterable<String> get inputExtensions => [".main"];

  testZip(String name) async {
    var fileName = "out/$name";
    var fileArchive =
    ZipDecoder().decodeBytes(File(fileName).readAsBytesSync());
    fileArchive.forEach((fileInArchive) {
      if (fileInArchive.name == "bootstrap") {
        print("File: $fileName/${fileInArchive.name} \n\t\t: ${fileInArchive
            .mode} -> ${fileInArchive.mode.toRadixString(8)} -> ${fileInArchive
            .mode.toRadixString(2).padLeft(32, '0')}\n" +
            "\t\t: ${fileInArchive.mode << 16} -> ${(fileInArchive.mode << 16)
                .toRadixString(8)} -> ${(fileInArchive.mode << 16)
                .toRadixString(2)
                .padLeft(32, '0')}");
      }
    });
  }

  @override
  FutureOr<void> build(PostProcessBuildStep buildStep) async {
    print("Step ZIP: $buildStep - ${buildStep.inputId.path}");

    var name = buildStep.inputId.pathSegments.last;
    var outputDir = "out/${name}";
    if (Directory(outputDir).existsSync()) {
      // clear old files
      Directory(outputDir).deleteSync(recursive: true);
    }
    Directory(outputDir).createSync(recursive: true);
    print("Current dir: " + Directory(".").absolute.path);

    var scriptFile = File("package_$name.sh");
    scriptFile.writeAsStringSync("""#!/bin/bash
echo "compiling..."
/usr/lib/dart/bin/dart2aot ${buildStep.inputId.path} ${outputDir}/$name.aot
echo "compiled"
""");

    print("Compiiling AOT file $name");
    await _runShellScript(scriptFile.path);
    scriptFile.deleteSync();
    print("AOT file $name compiled");
    prepareZip(name, outputDir);
    Directory(outputDir).deleteSync(recursive: true);
    return null;
  }

  Future<String> _copyResource(String name, String outputDir,
      String packagePath,
      {String packageName = "aws_lambda"}) async {
    var inputResource = resource.Resource(
        "package:$packageName/resources/$packagePath$name");

    var outputFile = File("$outputDir/$name");
    print("Coping ${inputResource.uri} to $outputFile");

    var data = await inputResource.readAsBytes();
    print("COping file size: ${data.length}");
    outputFile.writeAsBytesSync(data, flush: true);
    if (outputFile.existsSync()) {
      print("File copied: $outputFile");
      return outputFile.path;
    } else {
      print("File not copied.");
      return null;
    }
  }

  void _runShellScript(String pathToScript) async {
    print("Current dir: " + Directory(".").absolute.path);
    var randomName = Random().nextInt(1000).toString();
    if (Platform.isWindows) {
      print("Windows OS");
      var batFile = File("script_$randomName.bat");
      batFile.writeAsStringSync("""
  bash -c ./${pathToScript}
  exit
      """);
      print("Running BAT file ${batFile.path}");
      var result = Process.runSync(batFile.path, [],
          stderrEncoding: Encoding.getByName("UTF-8"),
          stdoutEncoding: Encoding.getByName("UTF-8"),
          runInShell: true,
          workingDirectory: ".");

      print("Exit code: ${result.exitCode}");
      batFile.deleteSync();
      print("output: " + result.stdout);
    } else if (Platform.isLinux || Platform.isMacOS) {
      var result = Process.runSync(pathToScript, [], runInShell: true);
      print("Exit code: ${result.exitCode}");
      print("output:" + result.stdout);
    }
    print("Script run completed: $pathToScript");
  }

  void prepareZip(String name, String outputDir) async {
    // copy zip files
    await _copyResource("bootstrap", outputDir, "dart_aot/");

    // prepare script to zip and change permission on bash
    var zipScript = """
#!/bin/bash

mkdir -p /tmp/$name/bin

echo "coping dartaotruntime to /tmp/$name/bin/"
cp /usr/lib/dart/bin/dartaotruntime /tmp/$name/bin/
echo "coping files... from $outputDir to /tmp/$name"
cp  $outputDir/bootstrap /tmp/$name/
cp  $outputDir/$name.aot /tmp/$name/main.dart.aot
echo "files copied"

chmod +x /tmp/$name/bootstrap
chmor +x /tmp/$name/bin/dartaotruntime
cd /tmp/$name
zip /tmp/$name/function.zip -r bin/ bootstrap main.dart.aot 
cd -

cp /tmp/$name/function.zip out/function_$name.zip

""";

    var scriptFile = File("package_$name.sh");
    scriptFile.writeAsStringSync(zipScript);
    _runShellScript(scriptFile.path);
    print("Run script!.");
    scriptFile.deleteSync();
  }

  // due to some issues with file permissions Archive can't be used.
  void prepareZipBugged(String name, String outputDir) async {
    // open resource zip
    var awsLambdaZip = resource.Resource(
        "package:aws_lambda_generator/resources/base_function.zip");

    var zipFileDecoder = ZipDecoder();

    Archive archive =
    zipFileDecoder.decodeBytes(await awsLambdaZip.readAsBytes());
    var aotFile = File("$outputDir/$name.aot");
    // add AOT zip file
    var aotArchiveFile = ArchiveFile("main.dart.aot", aotFile.lengthSync(),
        aotFile.readAsBytesSync().toList());
    archive.forEach((file) =>
        print(
            "File In ZIP:\\t $file, ${file.mode.toRadixString(8)}, ${file
                .unixPermissions.toRadixString(8)}"));
    archive.addFile(aotArchiveFile);
    // update ZIP permissions
    archive
        .findFile("bootstrap")
        .mode = int.parse("100777", radix: 8);
    archive
        .findFile("bin/dartaotruntime")
        .mode = int.parse("100777", radix: 8);

    var zipFileEncoder = ZipEncoder();
    File("out/aws_lambda-$name.zip")
        .writeAsBytesSync(zipFileEncoder.encode(archive));

    print("Clearing output dir");
    Directory(outputDir).deleteSync(recursive: true); // cleanup
  }
}

class ZipBuilder extends PostProcessBuilder {
  @override
  FutureOr<void> build(PostProcessBuildStep buildStep) async {
    print("Step ZIP: $buildStep - ${buildStep.inputId.path}");
    var name = buildStep.inputId.pathSegments.last;
    var outputDir = "out/${name}";
    if (Directory(outputDir).existsSync()) {
      Directory(outputDir).deleteSync(recursive: true);
    }

//    print(" test Dart sdk: ${String.fromEnvironment("DART_SDK")}");
    Directory(outputDir).createSync(recursive: true);
    print("Current dir: " + Directory(".").absolute.path);

    // build AOT command
    String pathAot;
    if (Platform.isWindows) {
      print("Windows OS");
      var result = Process.runSync("where", ["dart2aot"], stdoutEncoding: utf8);
      pathAot = (result.stdout as String).trim();
    } else if (Platform.isLinux || Platform.isMacOS) {
      print("LINUX or MAC OS");
      var result = Process.runSync("which", ["dart2aot"], stdoutEncoding: utf8);
      pathAot = (result.stdout as String).trim();
    }

    print("dart2aot path-> $pathAot");
    Process.runSync(
        pathAot, ["${buildStep.inputId.path}", "$outputDir/${name}.aot"]);
    var awsLambdaZip = resource.Resource(
        "package:aws_lambda_generator/resources/base_function.zip");

    // open resource zip
    var zipFileDecoder = ZipDecoder();
    Archive archive =
    zipFileDecoder.decodeBytes(await awsLambdaZip.readAsBytes());
    var aotFile = File("$outputDir/$name.aot");
    // add AOT zip file
    var aotArchiveFile = ArchiveFile("main.dart.aot", aotFile.lengthSync(),
        aotFile.readAsBytesSync().toList());
    archive.forEach((file) =>
        print("File In ZIP:\\t $file, ${file.mode}, ${file.unixPermissions}"));
    archive.addFile(aotArchiveFile);
    var zipFileEncoder = ZipEncoder();
    File("out/aws_lambda-$name.zip")
        .writeAsBytesSync(zipFileEncoder.encode(archive));

    Directory(outputDir).deleteSync(recursive: true); // cleanup
  }

  @override
  Iterable<String> get inputExtensions => [".main"];
}
