import 'dart:convert';
import 'dart:io';

import 'package:aws_lambda/aws_lambda.dart';
import 'package:aws_lambda/runtime/cognito_event.dart';

import 'handler.dart';
import 's3_event.dart';

typedef Handler = Future<AwsLambdaResponse> Function(
  AwsLambdaRequest request,
  Context context,
);

class NextInvocation {
  NextInvocation({
    this.httpClientResponse,
    this.inputData,
  });

  HttpClientResponse httpClientResponse;
  Map<String, dynamic> inputData;
}

class Context {
  Context(
    Map<String, String> environment,
    NextInvocation nextInvocation,
  ) {
    this.functionName = environment["AWS_LAMBDA_FUNCTION_NAME"] ?? '';
    this.functionVersion = environment["AWS_LAMBDA_FUNCTION_VERSION"] ?? '';
    this.logGroupName = environment["AWS_LAMBDA_LOG_GROUP_NAME"] ?? '';
    this.logStreamName = environment["AWS_LAMBDA_LOG_STREAM_NAME"] ?? '';
    this.memoryLimitInMB = environment["AWS_LAMBDA_FUNCTION_MEMORY_SIZE"] ?? '';
    this.awsRequestId = nextInvocation.httpClientResponse.headers
            .value('lambda-runtime-aws-request-id') ??
        '';
    this.invokedFunctionArn = nextInvocation.httpClientResponse.headers
            .value('lambda-runtime-invoked-function-arn') ??
        '';
    // final timeInterval = TimeInterval(responseHeaderFields["Lambda-Runtime-Deadline-Ms"] as! String)! / 1000
    // this.deadlineDate = Date(timeIntervalSince1970: timeInterval)
  }

  String functionName;
  String functionVersion;
  String logGroupName;
  String logStreamName;
  String memoryLimitInMB;
  String awsRequestId;
  String invokedFunctionArn;

  // DateTime deadlineDate;

  int getRemainingTimeInMillis() {
    // let remainingTimeInSeconds = deadlineDate.timeIntervalSinceNow
    // return Int(remainingTimeInSeconds * 1000)
    return 0;
  }
}

class Runtime {
  String awsLambdaRuntimeAPI;
  String handlerName;
  Map<String, Handler> handlers = {};
  Map<String, AwsS3Handler> s3Handlers = {};
  Map<String, AwsApiGatewayHandler> apiGatewayHandler = {};
  Map<String, AwsCognitoHandler> cognitoHandlers = {};

  // todo move to translator handler Map<String, ApiGatewayHandler> apiGatewayHandlers = {};
  Map<String, String> environment = Platform.environment;

  Runtime() {
    if (environment["AWS_LAMBDA_RUNTIME_API"] == null) {
      throw Exception('Missing Environment Variables: AWS_LAMBDA_RUNTIME_API');
    }
    if (environment["_HANDLER"] == null) {
      throw Exception('Missing Environment Variables: _HANDLER');
    }
    final handler = environment["_HANDLER"];
    this.awsLambdaRuntimeAPI = environment["AWS_LAMBDA_RUNTIME_API"];

    if (!handler.contains(".")) {
      throw Exception('Invalid Handler Name');
    }

    this.handlerName = handler.split('.')[1];
  }

  Future<NextInvocation> getNextInvocation() async {
    final request = await HttpClient().getUrl(
      Uri.parse(
        'http://$awsLambdaRuntimeAPI/2018-06-01/runtime/invocation/next',
      ),
    );
    final response = await request.close();
    final inputData = await parseBody(response);
    return NextInvocation(httpClientResponse: response, inputData: inputData);
  }

  Future<HttpClientResponse> postInvocationResponse(
      String requestId, dynamic body) async {
    final request = await HttpClient().postUrl(
      Uri.parse(
        'http://$awsLambdaRuntimeAPI/2018-06-01/runtime/invocation/$requestId/response',
      ),
    );
    request.add(
      utf8.encode(
        json.encode(body),
      ),
    );
    final response = await request.close();
    return response;
  }

  Future<HttpClientResponse> postInvocationError(
      String requestId, dynamic error) async {
    final request = await HttpClient().postUrl(
      Uri.parse(
        'http://$awsLambdaRuntimeAPI/2018-06-01/runtime/invocation/$requestId/error',
      ),
    );
    request.add(utf8.encode(error.toString()));
    final response = await request.close();
    return response;
  }

  Future<Map<String, dynamic>> parseBody(HttpClientResponse response) async {
    final data = await response.transform(Utf8Decoder()).toList();
    return json.decode(data.first) as Map<String, dynamic>;
  }

  void registerApiGatewayHandler(
      String name, AwsApiGatewayHandler handlerFunction) {
    apiGatewayHandler[name] = handlerFunction;
  }

  void registerCognitoHandler(String name, AwsCognitoHandler handlerFunction) {
    cognitoHandlers[name] = handlerFunction;
  }

  void registerHandler(String name, Handler handlerFunction) {
    handlers[name] = handlerFunction;
  }

  void registerS3Handler(String name, AwsS3Handler s3handlerFunction) {
    s3Handlers[name] = s3handlerFunction;
  }

  void start() async {
    Context context;
    NextInvocation nextInvocation;
    AwsLambdaResponse outputData;
    var counter = 0;

    do {
      try {
        nextInvocation = await getNextInvocation();
        context = Context(environment, nextInvocation);
        counter += 1;
        print("Invocation-Counter: $counter");

        final handler = handlers[handlerName];
        final s3Handler = s3Handlers[handlerName];
        final apiHandler = apiGatewayHandler[handlerName];
        final cognitoHandler = cognitoHandlers[handlerName];
        if (handler != null) {
          outputData = await handler(
            AwsLambdaRequest(nextInvocation.inputData),
            context,
          );
        } else if (s3Handler != null) {
          outputData = await s3Handler(
            AwsS3NotificationEvent.fromJson(nextInvocation.inputData),
            context,
          );
        } else if (apiHandler != null) {
          outputData = await apiHandler(
            ApiGatewayEvent.fromJson(nextInvocation.inputData),
            context,
          );
        } else if (cognitoHandler != null) {
          outputData = await cognitoHandler(
              AwsCognitoEvent.fromJson(nextInvocation.inputData), context);
        }
        if (handler == null && s3Handler == null && apiHandler == null) {
          throw Exception('Unknown Lambda Handler');
        }
        var responsePayload = outputData.body;
        // TODO: if you need X-Ray
        // if let lambdaRuntimeTraceId = responseHeaderFields["Lambda-Runtime-Trace-Id"] as? String {
        //     setenv("_X_AMZN_TRACE_ID", lambdaRuntimeTraceId, 0)
        // }

        await postInvocationResponse(context.awsRequestId, responsePayload);
      } on Exception catch (error) {
        await postInvocationError(context.awsRequestId, error);
      }
    } while (true);
  }
}
