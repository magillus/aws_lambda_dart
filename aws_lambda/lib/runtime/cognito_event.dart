import 'package:aws_lambda/aws_lambda.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cognito_event.g.dart';

typedef AwsCognitoHandler = Future<AwsLambdaResponse> Function(
    AwsCognitoEvent event, Context context);

@JsonSerializable()
class AwsCognitoEvent {
  String version;
  String triggerSource;
  String region;
  String userPoolId;
  String userName;
  AwsCognitoCallerContext callerContext;
  AwsCognitoRequest request;
  AwsCognitoResponse response;

  AwsCognitoEvent();

  factory AwsCognitoEvent.fromJson(Map<String, dynamic> json) =>
      _$AwsCognitoEventFromJson(json);

  Map<String, dynamic> toJson() => _$AwsCognitoEventToJson(this);
}

@JsonSerializable()
class AwsCognitoCallerContext {
  String awsSdkVersion;
  String clientId;

  AwsCognitoCallerContext();

  factory AwsCognitoCallerContext.fromJson(Map<String, dynamic> json) =>
      _$AwsCognitoCallerContextFromJson(json);

  Map<String, dynamic> toJson() => _$AwsCognitoCallerContextToJson(this);
}

@JsonSerializable()
class AwsCognitoRequest {
  Map<String, String> userAttributes = {};

  // from Pre Sign-up Lambda Trigger Parameters
  Map<String, String> validationData = {};
  Map<String, String> clientMetadata = {};

  // from Post Authentication Lambda Trigger Parameers
  bool newDeviceUsed;

  // from Pre Token Generation Lambda Trigger Parameters
  AwsGroupConfiguration groupConfiguration;

  // from Migrate User Trigger Parameters
  String password;

  // from Custom Message Lambda Trigger Parameters
  String codeParameter;

  AwsCognitoRequest();

  factory AwsCognitoRequest.fromJson(Map<String, dynamic> json) =>
      _$AwsCognitoRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AwsCognitoRequestToJson(this);
}

@JsonSerializable()
class AwsCognitoResponse {
  // from Pre Sign-up Lambda Trigger Parameters
  bool autoConfirmUser;
  bool autoVerifyPhone;
  bool autoVerifyEmail;
  AwsClaimOverrideDetails claimsOverrideDetails;
  Map<String, String> userAttributes = {};
  String finalUserStatus;
  String messageAction;
  List<String> desiredDeliveryMediums = [];
  bool forceAliasCreation;

  // from Custom Message Labmda Trigger
  String smsMessage;
  String emailMessage;
  String emailSubject;

  AwsCognitoResponse();

  factory AwsCognitoResponse.fromJson(Map<String, dynamic> json) =>
      _$AwsCognitoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AwsCognitoResponseToJson(this);
}

@JsonSerializable()
class AwsGroupConfiguration {
  List<String> groupsToOverride = [];
  List<String> iamRolesToOverride = [];
  String preferredRole;
  Map<String, String> clientMetadata = {};

  AwsGroupConfiguration();

  factory AwsGroupConfiguration.fromJson(Map<String, dynamic> json) =>
      _$AwsGroupConfigurationFromJson(json);

  Map<String, dynamic> toJson() => _$AwsGroupConfigurationToJson(this);
}

@JsonSerializable()
class AwsClaimOverrideDetails {
  Map<String, String> claimsToAddOrOverride = {};
  List<String> claimsToSuppress = [];
  AwsGroupConfiguration groupOverrideDetails;

  AwsClaimOverrideDetails();

  factory AwsClaimOverrideDetails.fromJson(Map<String, dynamic> json) =>
      _$AwsClaimOverrideDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$AwsClaimOverrideDetailsToJson(this);
}

/*
"claimsOverrideDetails": {
            "claimsToAddOrOverride": {
                "string": "string",
                . . .
            },
            "claimsToSuppress": ["string", . . .],

            "groupOverrideDetails": {
                "groupsToOverride": ["string", . . .],
                "iamRolesToOverride": ["string", . . .],
                "preferredRole": "string"
            }
        }

 */

/*
"groupConfiguration": {
        "groupsToOverride": ["string", . . .],
        "iamRolesToOverride": ["string", . . .],
        "preferredRole": "string",
        "clientMetadata": {
            "string": "string",
            . . .
        }*/

/*
{
    "version": number,
    "triggerSource": "string",
    "region": AWSRegion,
    "userPoolId": "string",
    "userName": "string",
    "callerContext":
        {
            "awsSdkVersion": "string",
            "clientId": "string"
        },
    "request":
        {
            "userAttributes": {
                "string": "string",
                ....
            }
        },
    "response": {}
}
 */
