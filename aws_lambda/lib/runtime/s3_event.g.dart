// GENERATED CODE - DO NOT MODIFY BY HAND

part of 's3_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AwsS3NotificationEvent _$AwsS3NotificationEventFromJson(
    Map<String, dynamic> json) {
  return AwsS3NotificationEvent(
    records: (json['Records'] as List)
        ?.map((e) => e == null
            ? null
            : AwsS3EventRecord.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AwsS3NotificationEventToJson(
        AwsS3NotificationEvent instance) =>
    <String, dynamic>{
      'Records': instance.records,
    };

AwsS3EventRecord _$AwsS3EventRecordFromJson(Map<String, dynamic> json) {
  return AwsS3EventRecord(
    eventName: json['eventName'] as String,
    eventSource: json['eventSource'] as String,
    awsRegion: json['awsRegion'] as String,
    eventTime: json['eventTime'] == null
        ? null
        : DateTime.parse(json['eventTime'] as String),
    eventVersion: json['eventVersion'] as String,
    userIdentity: json['userIdentity'] == null
        ? null
        : AwsUserIdentity.fromJson(
            json['userIdentity'] as Map<String, dynamic>),
    requestParameters: json['requestParameters'] == null
        ? null
        : RequestParameters.fromJson(
            json['requestParameters'] as Map<String, dynamic>),
    responseElements: json['responseElements'] == null
        ? null
        : ResponseElements.fromJson(
            json['responseElements'] as Map<String, dynamic>),
    s3: json['s3'] == null
        ? null
        : S3Data.fromJson(json['s3'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AwsS3EventRecordToJson(AwsS3EventRecord instance) =>
    <String, dynamic>{
      'eventVersion': instance.eventVersion,
      'eventSource': instance.eventSource,
      'awsRegion': instance.awsRegion,
      'eventTime': instance.eventTime?.toIso8601String(),
      'eventName': instance.eventName,
      'userIdentity': instance.userIdentity,
      'requestParameters': instance.requestParameters,
      'responseElements': instance.responseElements,
      's3': instance.s3,
    };

S3Data _$S3DataFromJson(Map<String, dynamic> json) {
  return S3Data(
    s3SchemaVersion: json['s3SchemaVersion'] as String,
    configurationId: json['configurationId'] as String,
    bucket: json['bucket'] == null
        ? null
        : S3Bucket.fromJson(json['bucket'] as Map<String, dynamic>),
    object: json['object'] == null
        ? null
        : S3Object.fromJson(json['object'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$S3DataToJson(S3Data instance) => <String, dynamic>{
      's3SchemaVersion': instance.s3SchemaVersion,
      'configurationId': instance.configurationId,
      'bucket': instance.bucket,
      'object': instance.object,
    };

S3Bucket _$S3BucketFromJson(Map<String, dynamic> json) {
  return S3Bucket(
    name: json['name'] as String,
    ownerIdentity: json['ownerIdentity'] == null
        ? null
        : AwsUserIdentity.fromJson(
            json['ownerIdentity'] as Map<String, dynamic>),
    arn: json['arn'] as String,
  );
}

Map<String, dynamic> _$S3BucketToJson(S3Bucket instance) => <String, dynamic>{
      'name': instance.name,
      'ownerIdentity': instance.ownerIdentity,
      'arn': instance.arn,
    };

S3Object _$S3ObjectFromJson(Map<String, dynamic> json) {
  return S3Object(
    key: json['key'] as String,
    size: json['size'] as int,
    eTag: json['eTag'] as String,
    sequencer: json['sequencer'] as String,
  );
}

Map<String, dynamic> _$S3ObjectToJson(S3Object instance) => <String, dynamic>{
      'key': instance.key,
      'size': instance.size,
      'eTag': instance.eTag,
      'sequencer': instance.sequencer,
    };

ResponseElements _$ResponseElementsFromJson(Map<String, dynamic> json) {
  return ResponseElements(
    xAmzRequestId: json['xAmzRequestId'] as String,
  );
}

Map<String, dynamic> _$ResponseElementsToJson(ResponseElements instance) =>
    <String, dynamic>{
      'xAmzRequestId': instance.xAmzRequestId,
    };

RequestParameters _$RequestParametersFromJson(Map<String, dynamic> json) {
  return RequestParameters(
    sourceIPAddress: json['sourceIPAddress'] as String,
  );
}

Map<String, dynamic> _$RequestParametersToJson(RequestParameters instance) =>
    <String, dynamic>{
      'sourceIPAddress': instance.sourceIPAddress,
    };

AwsUserIdentity _$AwsUserIdentityFromJson(Map<String, dynamic> json) {
  return AwsUserIdentity(
    principalId: json['principalId'] as String,
  );
}

Map<String, dynamic> _$AwsUserIdentityToJson(AwsUserIdentity instance) =>
    <String, dynamic>{
      'principalId': instance.principalId,
    };
