import 'package:json_annotation/json_annotation.dart';

import 'handler.dart';
import 'runtime.dart';

part 's3_event.g.dart';

typedef AwsS3Handler = Future<AwsLambdaResponse> Function(
  AwsS3NotificationEvent event,
  Context context,
);

@JsonSerializable()
class AwsS3NotificationEvent {
  @JsonKey(name: "Records")
  List<AwsS3EventRecord> records;

  AwsS3NotificationEvent({this.records});

  factory AwsS3NotificationEvent.fromJson(Map<String, dynamic> json) =>
      _$AwsS3NotificationEventFromJson(json);

  Map<String, dynamic> toJson() => _$AwsS3NotificationEventToJson(this);
}

@JsonSerializable()
class AwsS3EventRecord {
  String eventVersion;
  String eventSource;
  String awsRegion;
  DateTime eventTime;
  String eventName;
  AwsUserIdentity userIdentity;
  RequestParameters requestParameters;
  ResponseElements responseElements;
  S3Data s3;

  AwsS3EventRecord(
      {this.eventName,
      this.eventSource,
      this.awsRegion,
      this.eventTime,
      this.eventVersion,
      this.userIdentity,
      this.requestParameters,
      this.responseElements,
      this.s3});

  factory AwsS3EventRecord.fromJson(Map<String, dynamic> json) =>
      _$AwsS3EventRecordFromJson(json);

  Map<String, dynamic> toJson() => _$AwsS3EventRecordToJson(this);
}

@JsonSerializable()
class S3Data {
  String s3SchemaVersion;
  String configurationId;
  S3Bucket bucket;
  S3Object object;

  S3Data(
      {this.s3SchemaVersion, this.configurationId, this.bucket, this.object});

  factory S3Data.fromJson(Map<String, dynamic> json) => _$S3DataFromJson(json);

  Map<String, dynamic> toJson() => _$S3DataToJson(this);
}

@JsonSerializable()
class S3Bucket {
  String name;
  AwsUserIdentity ownerIdentity;
  String arn;

  S3Bucket({this.name, this.ownerIdentity, this.arn});

  factory S3Bucket.fromJson(Map<String, dynamic> json) =>
      _$S3BucketFromJson(json);

  Map<String, dynamic> toJson() => _$S3BucketToJson(this);
}

@JsonSerializable()
class S3Object {
  String key;
  int size;
  String eTag;
  String sequencer;

  S3Object({this.key, this.size, this.eTag, this.sequencer});

  factory S3Object.fromJson(Map<String, dynamic> json) =>
      _$S3ObjectFromJson(json);

  Map<String, dynamic> toJson() => _$S3ObjectToJson(this);
}

@JsonSerializable()
class ResponseElements {
  @JsonSerializable(fieldRename: FieldRename.kebab)
  String xAmzRequestId;

  ResponseElements({this.xAmzRequestId});

  factory ResponseElements.fromJson(Map<String, dynamic> json) =>
      _$ResponseElementsFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseElementsToJson(this);
}

@JsonSerializable()
class RequestParameters {
  String sourceIPAddress;

  RequestParameters({this.sourceIPAddress});

  factory RequestParameters.fromJson(Map<String, dynamic> json) =>
      _$RequestParametersFromJson(json);

  Map<String, dynamic> toJson() => _$RequestParametersToJson(this);
}

@JsonSerializable()
class AwsUserIdentity {
  String principalId;

  AwsUserIdentity({this.principalId});

  factory AwsUserIdentity.fromJson(Map<String, dynamic> json) =>
      _$AwsUserIdentityFromJson(json);

  Map<String, dynamic> toJson() => _$AwsUserIdentityToJson(this);
}

/*
{
  "Records": [
    {
      "eventVersion": "2.1",
      "eventSource": "aws:s3",
      "awsRegion": "us-east-2",
      "eventTime": "2019-09-03T19:37:27.192Z",
      "eventName": "ObjectCreated:Put",
      "userIdentity": {
        "principalId": "AWS:AIDAINPONIXQXHT3IKHL2"
      },
      "requestParameters": {
        "sourceIPAddress": "205.255.255.255"
      },
      "responseElements": {
        "x-amz-request-id": "D82B88E5F771F645",
        "x-amz-id-2": "vlR7PnpV2Ce81l0PRw6jlUpck7Jo5ZsQjryTjKlc5aLWGVHPZLj5NeC6qMa0emYBDXOo6QBU0Wo="
      },
      "s3": {
        "s3SchemaVersion": "1.0",
        "configurationId": "828aa6fc-f7b5-4305-8584-487c791949c1",
        "bucket": {
          "name": "lambda-artifacts-deafc19498e3f2df",
          "ownerIdentity": {
            "principalId": "A3I5XTEXAMAI3E"
          },
          "arn": "arn:aws:s3:::lambda-artifacts-deafc19498e3f2df"
        },
        "object": {
          "key": "b21b84d653bb07b05b1e6b33684dc11b",
          "size": 1305107,
          "eTag": "b21b84d653bb07b05b1e6b33684dc11b",
          "sequencer": "0C0F6F405D6ED209E1"
        }
      }
    }
  ]
}
 */
