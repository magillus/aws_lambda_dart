# aws_lambda

Test AWS Lambda with Dart AOT.

## Getting Started

See `example` directory for more details.

Add package dependency to `pubspec.yaml`:
```
   dependencies:
    aws_lambda: # <version when released or empty>
```

Create `lib/main.dart` file that runs your functions code.

Inside the file, import package.
`
import 'package:aws_lambda/aws_lambda.dart';
`

Prepare main method to register handler for function to the Runtime.

```

void main() async {
  await Runtime()
    ..registerHandler(
      'squareNumber',
      squareNumber,
    )
    ..start();
}

Map<String, dynamic> squareNumber(Map<String, dynamic> data, Context context) {
  final number = data['number'];
  var squareNumber = number * number;
  return {'result': squareNumber};
}

```

There is optional `ApiGatewayHandler apiGatewayHandler` parameter if defined you can customize API event implementation.
If that is not defined that ApiGatewayHandler will reuse same as normal handler approach.


## Compile and ZIP preparation

_Note: Make sure that your DART SDK is installed in your path_
Run below commands from root of your project (or `example/`):

-  copy `aws_lambda/reources` to your project's `resources` - **TODO: we need automate this build process.**
-  `dart2aot lib/main.dart resources/dart_aot/main.dart.aot` - this will compile and prepare AOT package into the folder
-  copy any resources or local files to `resources/dart_aot/*` directory
-  change active directory to `cd resources/dart_aot`
-  zip all with `zip -r example_function.zip bin/ bootstrap main.dart.aot` _note: attach any resources to zip file_
-  remove temporary files: `rm resources/dart_aot/main.dart.*`
-  Upload ZIP file to AWS Lambda
-  Use the handler as `main.<your function name>`  - function name taken from `registerHandle` function

## Keep `resources/dart_aot/bin/dartaotruntime` file up to date

The file is copied from /usr/lib/dart/bin/dartaotruntime on linux_64 SDK file.
It would be great to have it autoupdating - maybe future CI will do it.

