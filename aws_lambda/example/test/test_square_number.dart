import 'dart:convert';
import 'dart:io';

import 'package:aws_lambda/aws_lambda.dart';
import 'package:test/test.dart';

import '../lib/square_number.dart';

void main() {
  test('Test Square number - normal handler', () async {
    var data = AwsLambdaRequest({'number': 3});
    // todo fix test
    var response = await squareNumber(data, null);
    expect(response.body['result'], 9);
  });

  test('Test APIGateway', () async {
    var jsonSampleEvent =
    File("test/resources/api_gateway_event.json").readAsStringSync();
    var event = ApiGatewayEvent.fromJson(jsonDecode(jsonSampleEvent));
    var response = await rootNumber(event, null);
    expect(response.body["result"], 3);
  });

  test('Test S3 Event', () async {
    var jsonSampleEvent =
    File("test/resources/s3_notification_event.json").readAsStringSync();
    var event = AwsS3NotificationEvent.fromJson(jsonDecode(jsonSampleEvent));
    var response = await sizeInMegaBytes(event, null);
    expect(response.body["result"], 1.2446470260620117);
  });
}
